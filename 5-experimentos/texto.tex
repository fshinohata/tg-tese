\chapter{Experiments}
\label{ch:experiments}

\graphicspath{{\currfiledir/img/}}

\section{Environment}
All experiments explored in the next sections are
done using a testing dataset with 8000 synthetic
license plates, with none of them appearing in the
training dataset. For this chapter, the networks are named
\textit{SRConvNet} (plain CNN) and \textit{SRResNet} (residual CNN).

\section{Image quality comparison}
In this section, a simple image quality comparison
was done, without considering character readability.
For this task, the following loss metrics were used:
Mean Absolute Error (MAE), Mean Square Error (MSE),
Peak Signal-to-Noise Ratio (PSNR), Structural
Similarity (SSIM) and VGG loss
\citep{sr-2014-vgg}. The implementation of the
VGG content loss is taken from \citep{sr-2020-gan}.

In general, both networks achieved good fidelity for
each of the 8 license plate templates. The
Table~\ref{tab:5-image-quality} shows the results obtained
for both networks. The values for each loss is the mean
of all 8000 evaluations.

\begin{table}[!htp]
    \centering
    \caption{Image quality evaluation results for both networks. Best values are marked with bold text.}
    \label{tab:5-image-quality}
    \begin{tabular}{|c|c|c|c|c|c|}
        \hline
        \textbf{Network} & \textbf{MAE} & \textbf{MSE} & \textbf{PSNR} & \textbf{SSIM} & \textbf{VGG} \\
        \hline
        SRResNet & 0.0212 & 0.0016 & \textbf{28.5283} & \textbf{0.9595} & 0.0530 \\
        \hline
        SRConvNet & \textbf{0.0204} & \textbf{0.0015} & 28.4423 & 0.9583 & \textbf{0.0508} \\
        \hline
    \end{tabular}
\end{table}

% SRResNet
% mae: 0.0212984848767519
% mse: 0.001603280776180327
% psnr: 28.528337478637695
% ssim: 0.9594957232475281
% vgg: 0.053008660674095154

% SRConvNet
% mae: 0.020416174083948135
% mse: 0.0015738923102617264
% psnr: 28.44228744506836
% ssim: 0.9583128094673157
% vgg: 0.0508553683757782

As shown in the table, both networks achieved very close averages.
The plain CNN had a surprinsingly better
performance when it comes to replicating the overall structure
of the license plates. The speculation as to why the plain CNN
is better at replicating the ground truth image is because
it can simply copy the pixel values it learned during training
rather than calculating the proper residue that will result
in the ground truth plate.

\section{Character Readability}
As the section name suggests, here will be presented a showcase
of the overall character readability problems of each network.
From what was already told in Section~\ref{sec:4-training},
the results are expected to include character overlappings,
because the VGG loss function used for training does not
enforce character readability enough.
Also, it was expected that the plain CNN would end up
using more character overlappings than the residual CNN.
This fact can be observed when comparing the Figures
\ref{fig:5-srconvnet-predictions} and \ref{fig:5-srresnet-predictions}.
Note that the predictions from the SRConvNet present much
heavier overlappings.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{srconvnet-predictions.png}
    \caption{Predictions of the SRConvNet for a few random samples of each license plate template. The last two samples are real LPs, not present in the training dataset.}
    \label{fig:5-srconvnet-predictions}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{srresnet-predictions.png}
    \caption{Predictions of the SRResNet for a few random samples of each license plate template. The last two samples are real LPs, not present in the training dataset.}
    \label{fig:5-srresnet-predictions}
\end{figure}

It can also be observed that the synthetic LPs do not
represent real world scenarios with enough fidelity.
The last sample shown in the Figures
\ref{fig:5-srconvnet-predictions} and \ref{fig:5-srresnet-predictions}
show how poorly the training dataset is, when considering
that the networks will be used in real world scenarios.

One interesting fact is that since none of the networks
saw the real license plate example, they instead change
the output to the nearest template that they did see during
the training phase.

\section{Conclusion}
The experiments show that both CNN architectures can replicate
structural details of license plates with very high image fidelity.
However, when it comes to text fidelity and character readability,
since the loss function does not enforce this factor enough,
both networks decide to use overlappings when they cannot decide
on which character to place. This happens especially for similar
numbers like 0, 6, 8 and 9.

The performance comparison over character readability also shows
that the residual CNN has much better generalisation capabilities.
You can observe it by the fact that the character which the network
is most confident to be the correct one is "drawn" with much
stronger colors, while the overlappings of other characters
remain opaque. The plain CNN does not use stronger colors based
on its prediction, resulting in almost unreadable characters
(\textit{e.g.} the first character of the MRV-5646 plate).

Also, testing the networks with a simple, frontal and clear picture
of a "real world" license plate shows that the synthetic ones
are not good enough to substitute the usage of actual, real footage
for training them for real world applications.
