\section{Introduction}
Roses are red, violets are blue. There is no introduction too.

\section{Related Works}

Super-resolution is an ill-posed problem, simply because a low resolution (LR) image lacks information about the high resolution (HR) counterpart. We can say that the amount of lacking information is proportional to the ``distance'' (\textit{i.e.} resolution difference) between the LR image and the HR one.

Because of that, it is not simple to formulate an exact, mathematical method that maps a LR samples to a HR counterpart: there is more than one ``correct'' choice. For that reason, we need to rely on approximation methods. These methods, however, require some way to extract more information from the LR image.

One option is to use a set of similar (not equal) LR images that contain the area that you wish to super-resolve. Methods that use this approach are known as \textit{Multi-Frame Super Resolution}, and are commonly used when you have a low resolution video that needs to be super-resolved.

If you don not have a set of similar LR images to use, another option is to somehow learn to fill the missing details while maintaining a certain degree of similarity. In other words, you have to use your ``knowledge'' or ``experience'' and decide what pixels to add, and where to add. This approach is known as \textit{Single Image Super Resolution} (SISR), and usually uses trained neural network architectures such as Convolutional Neural Networks (CNNs) and Generative Adversarial Networks (GANs) \cite{sr-2017-progressive-vehicle-search-with-domain-priori-gan,sr-2018-multi-task-gan}.

\subsection{Multi-Frame Super Resolution Methods}

The earliest super resolution methods use frequency domain analysis on a set of frames to reconstruct a better image. In 1990, Kim \textit{et al} \cite{sr-1990-weighted-recursive-least-squares} proposed a recursive least squares reconstruction method applied to a set of shifted noisy LR frames of a same image. The method uses a Discrete Fourier Transform (DFT) on every LR frame, and then uses this transformed information to create a higher resolution image through a system of linear equations. Each iteration attempts to minimize an error norm when calculating the next estimated frame.

This method requires the shifts in the frames to be known, which is usually not the case in practical scenarios. Thus, in 1993 Bose \textit{et al} \cite{sr-1993-total-least-squares} proposed a modified version that accounted for errors in the shift estimations. Both methods do not account for degradation caused by blur, and the shifts in the images must be global, which restricts their practical use to very specific scenarios, such as super resolving satellite-taken photographs.

If you have local motion in your image set (an object moving, for instance), it means that the mentioned frequency domain methods cannot be applied, since any shift between two frames must be global. Unfortunately, local motion is expected in most real scenarios, especially in Automatic License Plate Recognition (ALPR) video samples. Thus, most works use spatial domain methods to deal with motion estimation and license plate (LP) alignment.

Jumping some years ahead, in 2007, Suresh \textit{et al} \cite{sr-2007-gd-gnc-mrf} proposed the use of a graduated non convexity algorithm to super-resolve license plate images. The method takes as input a set of license plate frames that are already cropped and aligned, and use a discontinuity-adaptive Markov random field (DAMRF) to iteratively reconstruct the HR image.

% ADICIONAR AQUI \cite{sr-2009-duality-based-tv-l1-optical-flow}

The DAMRF method of \cite{sr-2007-gd-gnc-mrf} was improved in 2012 by W. Zeng and X. Lu \cite{sr-2012-damrf}. Their proposal was a generalized DAMRF (GDAMRF), which does not require a fixed regularization parameter. At each iteration, the information gathered is used to adapt the algorithm. The GDAMRF is based on a bilateral filter, and is robust to noise and able to better preserve edges.

In 2015, Seibel \textit{et al} \cite{sr-2015-gsr} proposed a simple and effective spatial method, called Geometric K-Nearest Neighbors Super Resolution (GSR). Their aim was to achieve a good resolution whilst maintaining a low execution time. The method takes a set of shifted LR frames, which are then aligned with subpixel accuracy in a registration step. Finally, they project a HR grid over the aligned frames, and for each pixel in the HR grid, the k-closest ones are chosen to compose the HR pixel. Two years later, the same authors \cite{sr-2017-gsr-isr} presented a complete framework to recognize vehicle license plates in surveillance videos, in which they used a few variations of the proposed GSR method along with different tracking, registration and recognition algorithms.

\subsection{Single-Image Super Resolution Methods}

Repeating what was said in the beginning of this section, when you only have a single LR image to work with, some kind of prior knowledge becomes essential. There is a limit to what filters and estimations can provide when there is not enough information to use. Hence, using neural networks became a common choice in these scenarios.

In 2014, Chuang \textit{et al} \cite{sr-2014-simple-ann} proposed the use of a local binary pattern (LBP) feature extraction technique to train an Artificial Neural Network architecture. The objective is to super-resolve the LP characters independently. The network is fed with 59-dimensional feature vectors of multiple LR samples of a character, and their corresponding HR feature vector. The restoration process is iterative, and each iteration creates a better version of the image.

% ADICIONAR AQUI \cite{sr-2017-progressive-vehicle-search-with-domain-priori-gan}

In 2018, M. Zhang, W. Liu and H. Ma \cite{sr-2018-multi-task-gan} proposed a Multi-Task Generative Adversarial Network (MTGAN) architecture to execute both license plate super resolution and recognition altogether. The architecture consists of a Generative Network (GN) and a Discriminator Network (DN), just like regular GAN models. The main difference from the GAN model is that the DN does not try to set apart generated images from original ones. While the GN is trained to generate high resolution images, the DN judges whether the generated image has a good enough resolution to be recognized.

In 2019, Y. Zou \textit{et al} \cite{sr-2019-semantic-dictionaries} used sparse coding techniques to extract semantic information from vehicle license plates. The method consists of building pairs of sparse dictionaries $(D^c_h, D^c_l)$, in which $D^c_h$ is the high resolution dictionary with semantic information of the character $c$, and $D^c_l$ the low resolution counterpart. To build these dictionaries, it is necessary to use labeled LP datasets. The low resolution characters are scaled to the extremely low 6x3 resolution, and test results presented by the authors shows accuracy of over 97\%.

% ADICIONAR AQUI \cite{sr-2019-non-linear-regression-and-deep-learning}

\section{Datasets}

Due to the fact that super-resolution for license plates is used as part of a recognition system, there are not any datasets optimized for testing this specific task. Most published works use public Automatic License Plate Recognition (ALPR) databases for experimenting, and some use datasets built by the authors themselves. The usual approach is to create lower resolution samples by using blur, down-sampling and similar techniques.

There are numerous ALPR datasets, although most of them have a small number of samples. Table~\ref{table-datasets-information} describes a few datasets available, and what kind of samples they contain. Some of the datasets were not downloaded, and thus the information given is just a transcript of what was reported by the owners. Table~\ref{table-datasets-how-to-access} gives information about how to obtain these datasets.

The dataset with the biggest amount of samples is the Chinese City Parking Dataset (CCPD) \cite{lp-dataset-ccpd}. There are many different lighting, weather and tilt setups, and all this information is labeled. These traits makes it a good candidate for learning methods.

\begin{table}
    \centering
    \caption{General information about some ALPR datasets. The ones with ``Reported'' written above means that the dataset was not downloaded for analysis, and thus the data shown is what is reported in the dataset webpage.}
    \label{table-datasets-information}
    \begin{tabular}{c|c|c|c|c|c}
        \textbf{Dataset} &
            \textbf{\# Images} &
            \textbf{Size} &
            \textbf{Resolution} &
            \textbf{Format} &
            \textbf{Details}
    
        \\\hline
    
        OPEN-ALPR &
            \makecell{
                489 vehicles\\
                751 license plates\\
                1240 total} &
            395MB (ZIP) &
            Very varied &
            \makecell{
                PNG\\
                JPG} &
            \makecell{
                Benchmark dataset for OpenALPR.\\
                Contains BR, EU and US LPs.\\
                Most images are human readable.\\
                One image per vehicle/license plate.\\
                One vehicle/license plate per image.\\
                Mostly labeled.}
                
        \\\hline
        
        \makecell{(Reported)\\SSIG-ALPR \cite{lp-dataset-ssig-alpr}} &
            6660 total &
            35GB &
            1920x1080 &
            PNG &
            Brazilian LP dataset.

        \\\hline

        \makecell{(Reported)\\SSIG-SEGPLATE \cite{lp-dataset-ssig-segplate}} &
            - &
            8.6GB &
            1920x1080 &
            PNG &
            \makecell{
                Brazilian LP dataset.\\
                Only images of already\\
                segmented LP characters.}

        \\\hline

        \makecell{(Reported)\\UFPR-ALPR \cite{lp-dataset-ufpr-alpr}} &
            4500 total &
            - &
            - &
            - &
            \makecell{
                Brazilian LP dataset.\\
                3 different cameras used,\\
                1500 images taken with each one.}

        \\\hline

        \makecell{(Reported)\\VeRi-776 \cite{lp-dataset-veri-776-1,lp-dataset-veri-776-2,lp-dataset-veri-776-3}} &
            50000+ total &
            - &
            - &
            - &
            \makecell{
                Labeled.\\
                776 different vehicles.\\
                Proposed for vehicle re-identification\\
                purposes.}

        \\\hline

        Brno University HDR \cite{lp-dataset-czech} &
            652 total &
            64MB (ZIP) &
            Very varied &
            PNG &
            \makecell{
                Czech cropped LP dataset.\\
                Most images are human readable.\\
                Images of already cropped LPs.\\
                Varied angles.\\
                One image per license plate.\\
                Not labeled.}

        \\\hline

        Brno University ReId \cite{lp-dataset-czech} &
            185903 total &
            1.8GB (ZIP) &
            \makecell{
                Slightly varied around\\
                130x40} &
            PNG &
            \makecell{
                Czech cropped LP dataset.\\
                Most images are human readable.\\
                Images of already cropped LPs.\\
                Varied angles.\\
                6-30 frames per license plate.\\
                Not labeled.}

        \\\hline

        Rear View &
            510 total &
            32MB (ZIP) &
            640x480 &
            JPG &
            \makecell{
                Croatian LP dataset.\\
                Most images are human readable.\\
                Only rear view images.\\
                Small angle shifts.\\
                Not labeled.\\
                One image per vehicle.}

        \\\hline

        IRCP \cite{lp-dataset-iranian-1,lp-dataset-iranian-2} &
            220 total &
            82MB (ZIP) &
            \makecell{
                640x480\\
                800x600\\
                1024x768} &
            JPG &
            \makecell{
                Iranian LP dataset.\\
                Most images are human readable.\\
                Frontal and reat view of vehicles.\\
                Different perspectives, lighting\\
                and distances.\\
                Not labeled.}

        \\\hline

        \makecell{Chinese City\\Parking Dataset\\(CCPD)} \cite{lp-dataset-ccpd} &
            283037 total &
            12GB (.tar.bz2) &
            720x1160 &
            JPG &
            \makecell{
                Chinese LP dataset.\\
                Most images are human readable.\\
                One vehicle per image.\\
                One image per vehicle.\\
                Varied lighting, weather and tilt.\\
                Completely labeled.\\
                Labels have additional\\
                information about tilt\\
                degree, brightness, blur, etc}
    \end{tabular}
\end{table}

\begin{table}
    \centering
    \caption{Details of how to access the datasets.}
    \label{table-datasets-how-to-access}
    \begin{tabular}{c|p{5cm}|c|c}
        \textbf{Dataset} &
            \textbf{Link} &
            \textbf{License/Agreement} &
            \textbf{How to Obtain}
    
        \\\hline
    
        OPEN-ALPR &
            \url{https://github.com/openalpr/benchmarks} &
            \textit{GNU Affero General Public License v3.0} &
            Open

        \\\hline

        SSIG-ALPR \cite{lp-dataset-ssig-alpr} &
            \url{http://www.smartsenselab.dcc.ufmg.br/ssig-alpr-database} &
            - &
            \makecell{
                Request\\
                Requires signed agreement}

        \\\hline

        SSIG-SEGPLATE \cite{lp-dataset-ssig-segplate} &
            \url{http://www.smartsenselab.dcc.ufmg.br/ssig-segplate-database/} &
            - &
            \makecell{
                Request\\
                Requires signed agreement}

        \\\hline

        UFPR-ALPR \cite{lp-dataset-ufpr-alpr} &
            \url{https://web.inf.ufpr.br/vri/databases/ufpr-alpr/} &
            - &
            \makecell{
                Request\\
                Requires signed agreement}

        \\\hline

        VeRi-776 \cite{lp-dataset-veri-776-1,lp-dataset-veri-776-2,lp-dataset-veri-776-3} &
            \url{https://vehiclereid.github.io/VeRi/} &
            \makecell{
                Non-Commercial Use\\
                No Unauthorized Share} &
            Request

        \\\hline

        Brno University HDR \cite{lp-dataset-czech} &
            https://medusa.fit.vutbr.cz/traffic/research-topics/general-traffic-analysis/holistic-recognition-of-low-quality-license-plates-by-cnn-using-track-annotated-data-iwt4s-avss-2017/ &
            \makecell{
                Creative Commons\\
                Attribution-NonCommercial-ShareAlike 4.0\\ International} &
            Open

        \\\hline

        Brno University ReId \cite{lp-dataset-czech} &
            https://medusa.fit.vutbr.cz/traffic/research-topics/general-traffic-analysis/holistic-recognition-of-low-quality-license-plates-by-cnn-using-track-annotated-data-iwt4s-avss-2017/ &
            \makecell{
                Creative Commons\\
                Attribution-NonCommercial-ShareAlike 4.0\\ International} &
            Open

        \\\hline

        Rear View &
            \url{http://www.zemris.fer.hr/projects/LicensePlates/english/results.shtml} &
            - &
            Open
        
        \\\hline

        IRCP \cite{lp-dataset-iranian-1,lp-dataset-iranian-2} &
            \url{https://github.com/SeyedHamidreza/car_plate_dataset} &
            \textit{GNU General Public License v3.0} &
            Open

        \\\hline

        CCPD \cite{lp-dataset-ccpd} &
            \url{https://github.com/detectRecog/CCPD} &
            \textit{MIT License} &
            Open
    \end{tabular}
\end{table}

\section{Baseline Study}

In this section, we will take a better look at two of the earliest works about multi-frame image reconstruction \cite{sr-1990-weighted-recursive-least-squares,sr-1993-total-least-squares}. The analysis will focus on the computational complexity of the methods, and most of the mathematical formulations will be omitted.

\subsection{Recursive Reconstruction of High Resolution Image From Noisy Undersampled Multiframes}

The first work is an article from S.~P.~Kim, N. ~K.~Bose and H.~M.~Valenzuela \cite{sr-1990-weighted-recursive-least-squares}. The simplified problem formulation is the following:

Let $S$ be a set of noisy and shifted frames

\begin{equation}
\begin{gathered}
    f_k(i, j) = r_k(i, j) + n_k(i, j), \\
    i = 0, 1, \dots, M - 1, \\
    j = 0, 1, \dots, N - 1, \\
    k = 0, 1, \dots, p
\end{gathered}
\end{equation}

where $r_k$ is the denoised frame $k$ and $n_k$ is the zero-mean noise of frame $k$. Setting $f_0$ as the reference frame, then $f_1, f_2, \dots, f_p$ can be expressed as shifted versions of $f_0$ with independent noise

\begin{equation}
\begin{gathered}
    f_k(i, j) = r_0(i + \delta_{xk}, j + \delta_{yk}) + n_k(i, j)
\end{gathered}
\end{equation}

where $\delta_{xk}, \delta_{yk} \in \mathbf{R}$ are known shifts of frame $f_k$ relative to $f_0$. Note that these shifts are real numbers, whilst our frames are discrete. In the original formulation, the authors start by assuming that the frames $f_k$ are continuous. This fact can be simplified since the values of the frame $f_k$ are already given along with $\delta_{xk}$ and $\delta_{yk}$.

Finally, given $S$, the objective is to reconstruct a higher resolution and denoised frame $f$ of $f_0$.

The authors complete this task by using frequency domain analysis. They first transform the frames by using a Discrete 2-D Fourier Transform (DFT)

\begin{equation}
\label{eq:dft}
\begin{gathered}
    F_k(m,n) = \sum_{i = 0}^{M - 1} \sum_{l = 0}^{N - 1} f_k(i,l) \exp{\left( -2 \pi j \left( \dfrac{mi}{M} + \dfrac{nl}{N} \right) \right)}
\end{gathered}
\end{equation}

for $m = 0, 1, \dots, M - 1$, $n = 0, 1, \dots, N - 1$ and $k = 1, 2, \dots, p$, where $j = \sqrt{-1}$. Hence, our problem now resides in the complex domain.

In equation~\ref{eq:dft}, each element of the DFT $F_k$ is a function that depends on all elements of the frame $f_k$ (complexity $O(M^2N^2)$ for naive implementation).

The transformed frame $F_k$ is, in this case, the DFT of the corresponding noisy frame $f_k$. In the original formulation, the authors refer to $F_k$ as the frame without noise, and $Z_k = F_k + N_k$ as the DFT of the corrupted frame, where $N_k$ is the error term.

\newpage

Let $R_k$ be the DFT of the denoised frame $r_k$. Then, our corresponding formulation becomes

\begin{equation}
\label{eq:formulation}
\begin{gathered}
    F_k(m,n) = R_k(m, n) + N_k(m, n)
\end{gathered}
\end{equation}

for $m = 0, 1, \dots, M - 1$ and $n = 0, 1, \dots, N - 1$, where $N_k$ is the error term.

We want to somehow derive $R_k$ from equation~\ref{eq:formulation}, but we also do not know $N_k$. The authors propose a recursive reconstruction method to accomplish this, where each pixel is solved individually through a square system of linear equations that increases in size at every iteration.

This system comes from the aliasing relationship \cite{sr-1990-weighted-recursive-least-squares} that relates the continuous and discrete Fourier transforms by

\begin{equation}
\label{eq:aliasing-relationship-continuous}
\begin{gathered}
    R_k(m, n) =
    \dfrac{1}{T_x T_y}
    \sum_{i = -\infty}^{\infty}
    \sum_{l = -\infty}^{\infty}
    R^c_k \left( \dfrac{2 \pi m}{M T_x} + i \omega_x, \dfrac{2 \pi n}{N T_y} + l \omega_y \right)
\end{gathered}
\end{equation}

where $R^c_k$ is the continuous Fourier transform of the noise-free frame $r_k$, $T_x$ and $T_y$ are sampling periods along the $x$ and $y$ axes (today mostly referred to as step sizes), and

\begin{equation}
\begin{gathered}
    \omega_x = \dfrac{2 \pi}{T_x},\ \omega_y = \dfrac{2 \pi}{T_y}
\end{gathered}
\end{equation}

To be able to calculate equation~\ref{eq:aliasing-relationship-continuous}, the authors approximate the original continuous object by the band-limited constraint

\begin{equation}
\begin{gathered}
    | R^c(u, v) | = 0,\ |u| > L_x \omega_x \text{ and } |v| > L_y \omega_y
\end{gathered}
\end{equation}

resulting in the finite equation

\begin{equation}
\label{eq:aliasing-relationship-discrete}
\begin{gathered}
    R_k(m, n) =
    \dfrac{1}{T_x T_y}
    \sum_{i = -L_x}^{L_x}
    \sum_{l = -L_y}^{L_y}
    R^c_k \left( \dfrac{2 \pi m}{M T_x} + i \omega_x, \dfrac{2 \pi n}{N T_y} + l \omega_y \right)
\end{gathered}
\end{equation}

It is also important to remember that $r_k$ is defined as a shifted version of $r_0$, which means that the continuous Fourier transforms $R^c_k$ can also be expressed as a function of $R^c_0$

\begin{equation}
\label{eq:cft-of-k-as-function-of-zero}
\begin{gathered}
    R^c_k(u, v) = \exp{\left[ j 2 \pi (\delta_{xk} u + \delta_{yk} v) \right]} R^c_0(u, v)
\end{gathered}
\end{equation}

By applying equation~\ref{eq:cft-of-k-as-function-of-zero} into equation~\ref{eq:aliasing-relationship-discrete}, we obtain

\begin{equation}
\label{eq:linear-system-equation-form}
\begin{aligned}
    R_k(m, n) &= \\
    &\dfrac{1}{T_x T_y} \\
    &\sum_{i = -L_x}^{L_x}
    \sum_{l = -L_y}^{L_y}
    \exp{\left[ j 2 \pi (\delta_{xk} u + \delta_{yk} v) \right]} R^c_0 (u, v), \\
    u &= \left( \dfrac{2 \pi m}{M T_x} + i \omega_x \right) \\
    v &= \left( \dfrac{2 \pi n}{N T_y} + l \omega_y \right)
\end{aligned}
\end{equation}

And finally, we can express equation~\ref{eq:linear-system-equation-form} in the matrix form

\begin{equation}
\label{eq:linear-system-matrix-form-partial}
\begin{gathered}
    \begin{bmatrix} R_k(m, n) \end{bmatrix} = 
    \dfrac{1}{T_x T_y}
    \begin{bmatrix} \phi_{k,1} & \phi_{k, 2} & \dots & \phi_{k, 4 L_x L_y} \end{bmatrix}
    \\
    \begin{bmatrix}
    R^c_{mn}(1) \\
    R^c_{mn}(2) \\
    \vdots \\
    R^c_{mn}(4 L_x L_y)
    \end{bmatrix}
\end{gathered}
\end{equation}

where $R^c_{mn}(t)$, $t = 1, 2, \dots, 4 L_x L_y$ are the elements from the nested sum defined in equation~\ref{eq:linear-system-equation-form} mapped in column-major order

\begin{equation}
\begin{gathered}
    R^c_{mn}(t) = R^c_0 \left( \dfrac{2 \pi m}{M T_x} + i \omega_x, \dfrac{2 \pi n}{N T_y} + l \omega_y \right) \\
    i = (t-1) \mod (2L_x) - L_x \\
    l = \left\lfloor \dfrac{t - 1}{2 L_x} \right\rfloor - L_y
\end{gathered}
\end{equation}

and $\phi_{k,r}$ are the respective exponential elements

\begin{equation}
\label{eq:definition-phi-kt}
\begin{gathered}
    \phi_{k,t} = \exp{\left[ j 2 \pi \left( \delta_{xk} \left( \dfrac{2 \pi m}{M T_x} + i \omega_x \right) + \delta_{yk} \left( \dfrac{2 \pi n}{N T_y} + l \omega_y \right) \right) \right]}
\end{gathered}
\end{equation}

And by adding the frames $k = 1, 2, \dots, p$ to equation~\ref{eq:linear-system-matrix-form-partial}, we obtain the linear system

\begin{equation}
\label{eq:linear-system-matrix-form-complete}
\begin{gathered}
    \begin{bmatrix}
    R_1(m, n) \\
    R_2(m, n) \\
    \vdots \\
    R_p(m, n) \\
    \end{bmatrix} = 
    \dfrac{1}{T_x T_y}
    \begin{bmatrix}
    \phi_{1, 1} & \phi_{1, 2} & \dots & \phi_{1, 4 L_x L_y} \\
    \phi_{2, 1} & \phi_{2, 2} & \dots & \phi_{2, 4 L_x L_y} \\
    \vdots \\
    \phi_{p, 1} & \phi_{p, 2} & \dots & \phi_{p, 4 L_x L_y}
    \end{bmatrix}
    \\
    \begin{bmatrix}
    R^c_{mn}(1) \\
    R^c_{mn}(2) \\
    \vdots \\
    R^c_{mn}(4 L_x L_y)
    \end{bmatrix}
\end{gathered}
\end{equation}

Shortening the linear system from \ref{eq:linear-system-matrix-form-complete} as

\begin{equation}
\label{eq:linear-system-shortened}
\begin{gathered}
    R_p(m, n) = \phi\ R^c_{mn}
\end{gathered}
\end{equation}

it becomes visible that this system can be solved only when $p \geq 4 L_x L_y$. Also, $L_x, L_y \geq 1$, or the system would be degenerated. Hence, $p \geq 4$, meaning it is necessary to have at least 4 low resolution frames in order to reconstruct an image when $L_x = L_y = 1$.

With this system, we can restate the original problem from equation~\ref{eq:formulation} by substituting $R_k(m, n)$ with the new definition from \ref{eq:linear-system-shortened}:

\begin{equation}
\label{eq:formulation-2}
\begin{gathered}
    F_k(m,n) = \phi_k\ R^c_{mn} + N_k(m,n)
\end{gathered}
\end{equation}

Where $F_k$, $N_k$ and $\phi_k$ are, in this case, redefined as

\begin{equation}
\begin{gathered}
    F_k(m, n) =
    \begin{bmatrix}
    F_1(m, n) \\
    F_2(m, n) \\
    \vdots \\
    F_k(m, n) \\
    \end{bmatrix}
    N_k(m, n) = 
    \begin{bmatrix}
    N_1(m, n) \\
    N_2(m, n) \\
    \vdots \\
    N_k(m, n)
    \end{bmatrix} \\
    \phi_k = \dfrac{1}{T_x T_y}
    \begin{bmatrix}
    \phi_{1, 1} & \phi_{1, 2} & \dots & \phi_{1, 4L_xL_y} \\
    \phi_{2, 1} & \phi_{2, 2} & \dots & \phi_{2, 4L_xL_y} \\
    \vdots \\
    \phi_{k, 1} & \phi_{k, 2} & \dots & \phi_{k, 4L_xL_y}
    \end{bmatrix}
\end{gathered}
\end{equation}

In other words, $F_k(m, n)$ and $N_k(m, n)$ are redefined as $kx1$ vectors that contains the first $k$ values of $F_i(m, n)$ and $N_i(m, n)$, $i = 1, 2, \dots, k$, respectively, and $\phi_k$ is the $kx4L_xL_y$ matrix that contains the $k$ first $\phi_i$ rows.

Using the new formulation, the authors proceed to compute estimates for $R^c_{mn}$, and each new estimate is then added to the system and used to compute the next ones. The estimates aim to minimize the error norm

\begin{equation}
\begin{gathered}
    \begin{Vmatrix} E_k(m, n) \end{Vmatrix}^2 = 
    \left( F_k(m, n) - \phi_k \hat{R}^{(k)}_{mn} \right) * \left( F_k(m, n) - \phi_k \hat{R}^{(k)}_{mn} \right)
\end{gathered}
\end{equation}

where $*$ denotes ``complex conjugate transpose''. This is done for each pixel of the frame, which can become impractical very quickly.

After the final estimate is complete, the high resolution image is obtained by applying the inverse DFT. This image is a denoised version of the initial reference frame $f_0$, with details collected from the shifted $f_k$ frames in the frequency domain.

\subsection{Recursive Total Least Squares Algorithm for Image Reconstruction from Noisy, Undersampled Frames}

In 1993, two of the authors of \cite{sr-1990-weighted-recursive-least-squares} and H.~C.~Kim used the formulation from equation~\ref{eq:formulation-2} as starting point to work with the Recursive Total Least Square (RTLS) algorithm proposed by C.E. Davila \cite{sr-1991-davila}. This algorithm, however, was originally proposed to deal with the real number domain, so the authors had to adapt it to use complex data that is extracted in the frequency domain.

To include errors in the shift estimations, the authors redefine

\begin{equation}
\begin{gathered}
    \delta_{xk} = \overline{\delta}_{xk} + \Delta\delta_{xk}
\end{gathered}
\end{equation}

and apply it to the definition of the elements of $\phi_k$, becoming

\begin{equation}
\label{eq:definition-phi-kt-with-shift-error}
\begin{aligned}
    \phi_{k,t} = &\dfrac{1}{T_x T_y} \\
    &\exp{\left[ j 2 \pi \left( \overline{\delta}_{xk} \left( \dfrac{2 \pi m}{M T_x} + i \omega_x \right) + \overline{\delta}_{yk} \left( \dfrac{2 \pi n}{N T_y} + l \omega_y \right) \right) \right]} \\
    &\exp{\left[ j 2 \pi \left( \Delta\delta_{xk} \left( \dfrac{2 \pi m}{M T_x} + i \omega_x \right) + \Delta\delta_{yk} \left( \dfrac{2 \pi n}{N T_y} + l \omega_y \right) \right) \right]}
\end{aligned}
\end{equation}

which is then approximated as

\begin{equation}
\begin{gathered}
    \phi_{k,t} \approx \overline{\phi}_{k,t} + \epsilon_{k,t}
\end{gathered}
\end{equation}

and this approximation is used to define an coefficient error matrix $E_k$, resulting in the new formulation

\begin{equation}
\label{eq:formulation-rtls}
\begin{gathered}
    F_k(m,n) = (\phi_k + E_k)\ R^c_{mn} + N_k(m,n)
\end{gathered}
\end{equation}

Also, the minimization problem now becomes

\begin{equation}
\begin{aligned}
    &\text{minimize }
    \| [ N_k\ \vdots\ E_k ] \| \\
    &\text{subject to }
    F_k(m, n) - N_k(m, n) = (\phi_k + E_k)R^c_{mn}
\end{aligned}
\end{equation}

where $\| A \|$ denotes the Frobenius norm

\begin{equation}
\begin{gathered}
    \| A \|^2 = \sum_i \sum_j |a_{ij}|^2
\end{gathered}
\end{equation}

From this point onwards, the authors define their method to solve this minimization problem. It is stated that the results of both methods are very similar. The RTLS has the advantage of accounting for errors in the shift estimations.

\subsection{Conclusions about the methods}

One interesting fact about both methods \cite{sr-1990-weighted-recursive-least-squares,sr-1993-total-least-squares} is that they are adaptations of signal filtering and aliasing concepts, used in this case to denoise images instead of pure signals.

Although the computational complexity is high for both, most operations are matrix operations, which can be parallelized in order to speed up the processing phase.

\section{Conclusions}
WIP

\bibliographystyle{IEEEtran}
\bibliography{sibgrapi-2019}
