\chapter{Bibliography Review}
\label{ch:bibliography-review}

\graphicspath{{\currfiledir/img/}}

\section{Overview About Super Resolution}
Most pioneer methods crafted for image super resolution were
based on existing methods for denoising and "cleaning" of
wave signals. The published works of
\citep{sr-1990-weighted-recursive-least-squares, sr-1993-total-least-squares}
are one of the earliest to attempt applying signal denoising and
cleaning techniques to images. Their method works with a series of
copies of a image, where each copy can have its own noise, degradation
and global offset. The images are first converted to elements of
the wavenumber domain, where frequency domain analysis is applied
along with a recursive least square reconstruction algorithm
to obtain the high resolution sample.

These mathematical reconstruction methods usually
require multiple, slightly different samples of the
target image to add details in the HR output.
If only a single image is given, there is not much
information that can be recovered to recreate lost
details, which is the scenario of image rescaling
problems.

For cases where you have an image sequence,
it is possible to apply local motion estimation
and create compensated frames, and these frames
can be used to reconstruct a single HR frame
recursively \citep{sr-1998-avrin}.

Nowadays, most methods revolve around the
usage of deep artificial neural networks, especially
the convolutional neural network architecture.
By providing sufficient data to train a proposed
architecture, neural networks often give results
that are as good or better than existing non-ANN algorithms,
and most of the time, with less execution time (for
evaluations, not considering training time).

Still, many non-ANN works of the field also provide
good results. For example, \citep{sr-2019-semantic-dictionaries}
proposed a Sparse Coding Super Resolution method that builds
semantic dictionaries to super-resolve license plate characters
that were previously segmented.

In the scenario of multi-frame SR, \citep{sr-2015-gsr}
proposed a geometric K-nearest neighbor method to
super-resolve a low resolution image sequence. It makes use of
motion field estimation to compensate local motion
within each frame, aligning all frames with subpixel
accuracy in order to define the value of each pixel
in the HR grid.

For CNN approaches, \citep{sr-2017-motion-compensation-cnn}
proposed a solution for multi-frame super resolution that
also makes use of motion compensation, along with a simple
\textit{ResNet} architecture. Given a target central frame, the
algorithm takes a few frames that comes before and after
the target, applies motion compensation based on the central
frame and finally feeds these frames as input to the network.

\section{License Plate Super Resolution}
When targeting vehicle license plates for Super Resolution,
as mentioned on Section~\ref{sec:2-sr-license-plates},
the new restriction of character readability must be
considered when evaluating a proposed solution. If
the license plate has good quality but the characters
cannot be read, then the proposed solution is useless.

Considering character readability, there are a few
approaches that are used. One of them is simple
character super resolution, where the license plate
is generally ignored. In this case, only the characters
of the plate are focused as SR targets.
\citep{sr-2014-simple-ann} proposed an ANN architecture
that receives as input a character and outputs
its super-resolved version. Given a vehicle image,
the method first extracts the license plate, segments
its characters and finally feeds the segmented characters
to the ANN architecture.
Using a character SR approach has the advantage of
having a smaller image to deal with during training,
which can speed up the process significantly.

Another approach is to apply SR to a cropped license
plate. In this case, you have the problem of variability
of LP layouts, including color and the occasional presence
of figures, flags, and other details that may interfere
in the super resolution process and cause appearance of
artifacts in the output.
One such method is the one proposed by \citep{sr-2020-gan},
in which a residual GAN architecture is trained to super-resolve
license plate images with an extremely low resolution, as shown in
Figure~\ref{fig:3-sr-2020-gan-example}.

\begin{figure}
    \centering
    \includegraphics[scale=.9]{sr-2020-gan-example.png}
    \caption{Showcase of the capabilities of the model proposed by \citep{sr-2020-gan}. GT stands for Ground Truth.}
    \label{fig:3-sr-2020-gan-example}
\end{figure}

\section{Database Availability}
There are many datasets created for training
Automatic License Plate Recognition (ALPR) systems.
Unfortunately, most of them are not specialized for
super resolution, so in many cases you cannot obtain
a ground truth license plate image with a decently
high resolution and quality to use for training.

You can see detailed information about a few
datasets on Table~\ref{table:datasets-information}
and how to access each on Table~\ref{table:datasets-how-to-access}.
As said above, most of these datasets do not have close
up, high resolution images of the license plates. The
smallest distance that the photos are taken average to
about two meters (see CCPD dataset).

\begin{table}[!htp]
    \centering
    \caption{General information about some ALPR datasets. The ones with ``Reported'' written above means that the dataset was not downloaded for analysis, and thus the data shown is what is reported in the dataset webpage.}
    \label{table:datasets-information}
    \resizebox{\textwidth}{!}{
        \begin{tabular}{c|c|c|c|c|c}
            \textbf{Dataset} &
                \textbf{\# Images} &
                \textbf{Size} &
                \textbf{Resolution} &
                \textbf{Format} &
                \textbf{Details}
        
            \\\hline
        
            OPEN-ALPR &
                \makecell{
                    489 vehicles\\
                    751 license plates\\
                    1240 total} &
                395MB (ZIP) &
                Very varied &
                \makecell{
                    PNG\\
                    JPG} &
                \makecell{
                    Benchmark dataset for OpenALPR.\\
                    Contains BR, EU and US LPs.\\
                    Most images are human readable.\\
                    One image per vehicle/license plate.\\
                    One vehicle/license plate per image.\\
                    Mostly labeled.}
                    
            \\\hline
            
            \makecell{(Reported)\\SSIG-ALPR \citep{lp-dataset-ssig-alpr}} &
                6660 total &
                35GB &
                1920x1080 &
                PNG &
                Brazilian LP dataset.

            \\\hline

            \makecell{(Reported)\\SSIG-SEGPLATE \citep{lp-dataset-ssig-segplate}} &
                - &
                8.6GB &
                1920x1080 &
                PNG &
                \makecell{
                    Brazilian LP dataset.\\
                    Only images of already\\
                    segmented LP characters.}

            \\\hline

            \makecell{(Reported)\\UFPR-ALPR \citep{lp-dataset-ufpr-alpr}} &
                4500 total &
                9.7GB &
                1920x1080 &
                PNG &
                \makecell{
                    Brazilian LP dataset.\\
                    3 different cameras used,\\
                    1500 images taken with each one.\\
                    Labeled.}

            \\\hline

            \makecell{(Reported)\\VeRi-776 \citep{lp-dataset-veri-776-1,lp-dataset-veri-776-2,lp-dataset-veri-776-3}} &
                50000+ total &
                - &
                - &
                - &
                \makecell{
                    Labeled.\\
                    776 different vehicles.\\
                    Proposed for vehicle re-identification\\
                    purposes.}

            \\\hline

            Brno University HDR \citep{lp-dataset-czech} &
                652 total &
                64MB (ZIP) &
                Very varied &
                PNG &
                \makecell{
                    Czech cropped LP dataset.\\
                    Most images are human readable.\\
                    Images of already cropped LPs.\\
                    Varied angles.\\
                    One image per license plate.\\
                    Not labeled.}

            \\\hline

            Brno University ReId \citep{lp-dataset-czech} &
                185903 total &
                1.8GB (ZIP) &
                \makecell{
                    Slightly varied around\\
                    130x40} &
                PNG &
                \makecell{
                    Czech cropped LP dataset.\\
                    Most images are human readable.\\
                    Images of already cropped LPs.\\
                    Varied angles.\\
                    6-30 frames per license plate.\\
                    Not labeled.}

            \\\hline

            Rear View &
                510 total &
                32MB (ZIP) &
                640x480 &
                JPG &
                \makecell{
                    Croatian LP dataset.\\
                    Most images are human readable.\\
                    Only rear view images.\\
                    Small angle shifts.\\
                    Not labeled.\\
                    One image per vehicle.}

            \\\hline

            IRCP \citep{lp-dataset-iranian-1,lp-dataset-iranian-2} &
                220 total &
                82MB (ZIP) &
                \makecell{
                    640x480\\
                    800x600\\
                    1024x768} &
                JPG &
                \makecell{
                    Iranian LP dataset.\\
                    Most images are human readable.\\
                    Frontal and reat view of vehicles.\\
                    Different perspectives, lighting\\
                    and distances.\\
                    Not labeled.}

            \\\hline

            \makecell{Chinese City\\Parking Dataset\\(CCPD)} \citep{lp-dataset-ccpd} &
                283037 total &
                12GB (.tar.bz2) &
                720x1160 &
                JPG &
                \makecell{
                    Chinese LP dataset.\\
                    Most images are human readable.\\
                    One vehicle per image.\\
                    One image per vehicle.\\
                    Varied lighting, weather and tilt.\\
                    Completely labeled.\\
                    Labels have additional\\
                    information about tilt\\
                    degree, brightness, blur, etc}
        \end{tabular}
    }
\end{table}

\begin{table}[!htp]
    \centering
    \caption{Details of how to access the datasets.}
    \label{table:datasets-how-to-access}
    \resizebox{\textwidth}{!}{
        \begin{tabular}{c|p{5cm}|c|c}
            \textbf{Dataset} &
                \textbf{Link} &
                \textbf{License/Agreement} &
                \textbf{How to Obtain}
        
            \\\hline
        
            OPEN-ALPR &
                \url{https://github.com/openalpr/benchmarks} &
                \textit{GNU Affero General Public License v3.0} &
                Open

            \\\hline

            SSIG-ALPR \citep{lp-dataset-ssig-alpr} &
                \url{http://www.smartsenselab.dcc.ufmg.br/ssig-alpr-database} &
                - &
                \makecell{
                    Request\\
                    Requires signed agreement}

            \\\hline

            SSIG-SEGPLATE \citep{lp-dataset-ssig-segplate} &
                \url{http://www.smartsenselab.dcc.ufmg.br/ssig-segplate-database/} &
                - &
                \makecell{
                    Request\\
                    Requires signed agreement}

            \\\hline

            UFPR-ALPR \citep{lp-dataset-ufpr-alpr} &
                \url{https://web.inf.ufpr.br/vri/databases/ufpr-alpr/} &
                - &
                \makecell{
                    Request\\
                    Requires signed agreement}

            \\\hline

            VeRi-776 \citep{lp-dataset-veri-776-1,lp-dataset-veri-776-2,lp-dataset-veri-776-3} &
                \url{https://vehiclereid.github.io/VeRi/} &
                \makecell{
                    Non-Commercial Use\\
                    No Unauthorized Share} &
                Request

            \\\hline

            Brno University HDR \citep{lp-dataset-czech} &
                https://medusa.fit.vutbr.cz/traffic/research-topics/general-traffic-analysis/holistic-recognition-of-low-quality-license-plates-by-cnn-using-track-annotated-data-iwt4s-avss-2017/ &
                \makecell{
                    Creative Commons\\
                    Attribution-NonCommercial-ShareAlike 4.0\\ International} &
                Open

            \\\hline

            Brno University ReId \citep{lp-dataset-czech} &
                https://medusa.fit.vutbr.cz/traffic/research-topics/general-traffic-analysis/holistic-recognition-of-low-quality-license-plates-by-cnn-using-track-annotated-data-iwt4s-avss-2017/ &
                \makecell{
                    Creative Commons\\
                    Attribution-NonCommercial-ShareAlike 4.0\\ International} &
                Open

            \\\hline

            Rear View &
                \url{http://www.zemris.fer.hr/projects/LicensePlates/english/results.shtml} &
                - &
                Open
            
            \\\hline

            IRCP \citep{lp-dataset-iranian-1,lp-dataset-iranian-2} &
                \url{https://github.com/SeyedHamidreza/car_plate_dataset} &
                \textit{GNU General Public License v3.0} &
                Open

            \\\hline

            CCPD \citep{lp-dataset-ccpd} &
                \url{https://github.com/detectRecog/CCPD} &
                \textit{MIT License} &
                Open
        \end{tabular}
    }
\end{table}

\section{Conclusion}
Most recent works use convolutional neural network
architectures to super resolve images and videos.
For license plates, the preferred architecture is
the GAN architecture, due to its discriminator
network being able to prevent the generator network
from "cheating" by using overlapped characters when
generating the output image, as well as enforcing it
to keep the license plate structure.

In the context of super resolving an entire license plate
(which is often what clients desire), the variability of
colors, layouts and details of LPs makes this problem
extremely hard to generalise, since there is not many
datasets with high resolution footage available. Hence,
acquiring a good amount of training data is, by itself,
a challenge.
