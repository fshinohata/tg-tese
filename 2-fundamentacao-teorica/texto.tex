\chapter{Background}

\graphicspath{{\currfiledir/img/}}

% --------------------------------------
\section{Super Resolution}
\label{sec:sr}
Super Resolution refers to a technique that increases the resolution
of images based on one or multiple samples. The main objective is
to obtain an image with higher resolution, while also achieving
better quality. For that reason, simple image rescaling algorithms
like bicubic and bilinear interpolation are not classified
as super resolution algorithms.

When a better quality is also desired, rescaling solutions are
switched with recursive reconstruction, convolutional neural
network and other methods that allow more details to be generated
in the final high resolution sample.

Like image rescaling, works related to super resolving or
"enhancing" images are not new. It is considered as an ill-posed
problem, since low resolution (LR) images lack information
about the high resolution (HR) counterpart.

The ill-posed part is closely related to a surjective function inversion
problem, since multiple HR images end up being mapped (or undersampled)
to the same LR image, because the HR set is bigger than the LR set.
In this context, super resolving a LR image means selecting one of the HR images
from the inversion of the undersampling function.

Of course, this is not a trivial problem. You need something to counter the
lack of information in order to achieve good fidelity. One option is to use
a set of similar LR images that contain the "area" that you wish to super
resolve. This method is called \textit{Multi-Frame Super Resolution}, and
it is commonly used when you have low resolution video footage or many
images of the same thing with slightly different angles. Some examples are
the works of \citep{sr-1998-avrin, sr-2012-damrf, sr-2015-gsr,
sr-2017-motion-compensation-cnn}.

In cases where you do not have access to a set of images containing the
target object to be super resolved (\textit{i.e.} you only have one image
to extract the data from), there is the problem of how to fill the HR pixel
grid. The technique that super resolve images this way is called
\textit{Single-Frame Super Resolution}. Some examples are the works of
\citep{sr-2014-simple-ann, sr-2019-semantic-dictionaries, sr-2020-gan,
sr-2019-denoise}.

\subsection{Multi-Frame Super Resolution}
This kind of method is suitable for cases where you have access
to either a sequence of images or video footage, with low
difference between each element in the sequence.
As the name says, it uses multiple, similar samples in order
to reconstruct a single, higher resolution one.

There are many factors that can lead to the acquisition of low quality
images or video footage such as hardware quality, interference
during transmission, and lack of storage space for higher quality data.

Satellite data, for example, is transmitted wirelessly, and thus is
much more vulnerable to signal corruption. Satellite imagery usually
forms a sequence with overlapping areas, and these can be
used to collect extra information about the image in order to generate
another image with more details (\textit{i.e.} a super resolved version).
\citep{sr-1990-weighted-recursive-least-squares} explored this topic in
detail, also considering noise in the image sequence.

In this scenario, if the satellite is taking photos with little or
no zooming at all, the images taken will have almost nonexistent
local motion between each sample in the sequence. Hence, using
motion field estimation is not necessary considering that each
image in the sequence was taken within a short time of each other.

Another common scenario is footage from security cameras.
They usually use very low bitrates during recording in order to minimize
storage usage, generating high resolution footage with very poor quality.
In this case, super resolving the footage requires local motion to be
considered, especially if the camera is recording a road or sidewalk.

This scenario is especially tricky because the low quality makes
detection of object edges a very challenging problem. Without
proper detection of the objects, estimating motion fields to
compensate the images can lead to weird artifacts appearing
in the super resolved version.

\subsection{Single-Frame Super Resolution}
As the name suggests, single-frame super resolution attempts
to obtain a higher resolution image based on a single sample.
Image rescaling algorithms solve this problem without generating
new information, while neural network based methods use the knowledge of
the network to fill the pixel grid, thus generating information that may
not be present in the LR image.

% --------------------------------------
\section{Convolutional Neural Network (CNN)}
A CNN is a type of Artificial Neural Network (ANN)
designed specifically for image processing. In other words,
its structure assumes that the input will consists of images.
The overall architecture of a CNN has three types of layers:
convolutional layer, pooling layer and fully-connected layers.
By stacking these layers you obtain a CNN architecture
\citep{sr-2015-cnn-introduction}.

Like other ANNs, a Convolutional Neural Network does
its hyperparameter tuning by using a loss function
for backpropagation. Since the input is assumed to be
a image, the usual choices for the loss function are
the ones that can give the best evaluation about
what is expected of the output image. For SR using single
CNNs, some commonly used loss functions are
Mean Absolute Error (MAE),
Mean Square Error (MSE),
Structural Similarity (SSIM) and
Peak Signal-to-Noise Ratio (PSNR).
There are also loss functions specialized for other
architectures, like the Adversarial Loss, crafted for
training a Generative Adversarial Network (GAN).

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=.8]{cnn-example.png}
	\caption{Example of a CNN with four layers \citep{sr-2015-cnn-introduction}.}
	\label{fig:cnn-example}
\end{figure}

\subsection{The convolutional layer}
This layer is the main "component" of a CNN. It uses
learnable kernels followed by an activation function
(commonly ReLU) to process the input into a 2D activation
map. A \textbf{kernel} is a low dimensionality vector that
is applied by "sliding" it along the input vector,
as shown in Figure~\ref{fig:cnn-kernel}.

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=.8]{cnn-kernel.png}
	\caption{
		Visual representation of a convolutional layer.
		The kernel is placed on the input vector
		and then used to calculate a weighted sum of
		itself and the nearby pixels \citep{sr-2015-cnn-introduction}.
	}
	\label{fig:cnn-kernel}
\end{figure}

The result, as said before, will be a new vector whose
values form what is called a 2D activation map. These maps
store characteristics deemed important by the CNN during
the training phase.

\subsection{The pooling layer}
This layer is responsible for aggregating the data
from the activation map (obtained by the convolutional
layer) and reducing its dimensionality, usually by
using the MAX function or a normalization function like
L1 or L2.

Since it reduces the complexity of the incoming input,
we can say that this layer is destructive, and thus
there is a need to be cautious about how much you
want to simplify your data.

\subsection{The fully-connected layer}
It is a layer that contains neurons that are directly
connected to the neurons in its adjacent layers, just like
a regular ANN layer. Each neuron has a matrix of weights
and bias that are adjusted during the training phase
to transform the input to the expected output.

This is the part where the input is
processed and classified. In most cases, you want to
feed this layer the output of a pooling layer, as
the complexity of the input will then be reduced,
requiring less overall neurons to be trained. The
greatly reduced amount of neurons required to train
in this layer is the main difference between a CNN and
an ANN. For super resolution, however, this layer
is not used, since the desired output is an image
with the same or higher resolution than the input,
and not just a classification vector.

Attempting to use this layer for SR defeats the objective
of all prior steps that reduces the complexity of
the input, since the output dimensionality will
still require more neurons to be allocated to
allow the layer to properly calculate it.

\subsection{About Training}
Training a single CNN architecture is the same process as
any other singular ANN. Singular because some
architectures like Generative Adversarial Networks (GAN)
use more than one independent networks that need to
learn different things during training.

For generic Super Resolution, generally you just need
a decent size image dataset with a good variety of
environments. Each image on the dataset is
labeled as the ground truth, and a lower resolution
version is generated to use during the training phase.
The lower resolution images are fed to the CNN, and
its output images are then compared with the ground
truth versions. The same process is applied for "specialized"
Super Resolution. Only the training dataset
is changed to one that contains only the objects
that you wish your network to learn.

One advantage of training a CNN for Super Resolution
is the easiness of applying data augmentation.
Changing brightness, rotation, saturation and other
image aspects can be done quickly, and it is a
good way to artificially increase your dataset size
without compromising training time.

\section{Residual Convolutional Neural Network}
Also known as \textit{ResNet}, a residual CNN consists
of a non-linear layer architecture. Instead of one layer
$L_i$ receiving the output of the previous layer $L_{i-1}$
and sending its own output to the next layer $L_{i+1}$, a
residual network introduces \textit{skip connections}, which
are connections from a layer $L_i$ to another layer $L_k$, where
$k > i + 1$. In other words, the output of a layer is sent
to its adjacent layer and also to other layers further down
the network \citep{resnet}.

This architecture was proposed to mitigate the
problem of vanishing gradients, and its benefits include
better generalisation capabilities and overall better
accuracy for deep neural networks. It is very common
to see this architecture being used to solve SR problems
due to its good generalisation.

% --------------------------------------
\section{Super Resolution of Vehicle License Plates}
\label{sec:2-sr-license-plates}
On Section~\ref{sec:sr} an overall explanation about
generic Super Resolution was given. To recapitulate,
when the objective is to achieve a HR image with
better quality and detail, one of the factors
that have to be considered is the fidelity of the
resulting image with the ground truth.

To measure fidelity, many methods use measuring algorithms
like Mean Absolute Error (MAE) or Structural Similarity
(SSIM). Although these techniques do suffice when
you want your HR image to look like the ground
truth, unfortunately that is not the case for
vehicle license plates, due to the presence and
importance of its characters.

When you need to apply SR for any kind of text,
using MAE or SSIM to train your network will not
give you the fidelity needed when the image has
a poor quality. If the network is not able to
discern which character is displayed, it will
"merge" characters resulting in overlappings
\citep{sr-2020-gan}.
(see Figure~\ref{fig:cnn-overlapping-characters}).
This happens because doing so gives a lower
error than choosing a single character to put
in the output plate image. In other words, the
MAE and SSIM do not enforce character fidelity and
readability enough, and thus the network will not learn
to preserve their structure correctly.

\begin{figure}[!htb]
	\centering
	\includegraphics{cnn-sr-overlapping-character.png}
	\caption{Example of the output image of a SR-CNN. Notice how the network decided to overlap the characters that appears to be a "P" and an "L". Image taken from the GitHub page of \citep{sr-2020-gan}.}
	\label{fig:cnn-overlapping-characters}
\end{figure}

In order to avoid allowing the network to "cheat"
by using overlapped characters or using shapes
that only resembles a character, extra restrictions
need to be applied through the loss function.

One example is using a GAN architecture, where
the discriminator network learns to recognize
real license plate images. In this case, if
the generator network attempts to cheat by using
overlapped characters, the discriminator network
will not recognize the plate as real.

For single network architectures, the loss function
must be able to discern by itself if the license plate
contains invalid characters. This is a very
challenging problem, as it requires it being capable
of recognizing characters reliably. With a GAN,
this problem is solved by using a loss function
that mixes a structural/perceptual evaluation of the
output from the generator network with the evaluation
of the discriminator network \citep{sr-2020-gan}.

\subsection{Usability of License Plate Super Resolution systems}
Having a solution that is capable of super resolving
license plates with low resolution and quality has
a simple, direct usability: it can be used to
improved already existing and deployed ALPR solutions.

On a different note, some works attempt to create an
\textit{Automatic License Plate Recognition} system
that is able to read low resolution images directly.
One such example is the work of \citep{sr-alpr-2019},
which attempted to apply a multi-task learning method
to recognize license plate images with low resolution.
While this approach is also viable, it requires the
introduction of a brand new solution for this specific
scenario.

\section{Conclusion}
Super Resolution of License Plates shares an
interesting restriction with generic text Super
Resolution problem. Not only the fidelity, but
the character readability must also be considered
in any evaluation of an algorithm. If the image
has good detail but the characters are not readable
or do not make sense, then the super resolved image
becomes useless.

As discussed, common methods used today involve
training CNN architectures (single-network or GAN)
in an attempt to guarantee character readability
in the license plate, since crafting a "traditional"
mathematical approach is much more challenging due
to the need of the method being able to consider
character structure during the reconstruction
process.
